import resolve_url from './resolve_url.js';
import { err_as_text } from './tools.js';

// node.js polyfill
// requires dev dependencies
try {
  global.WebSocket = (await import("ws")).default;
} catch (_) { }

/*
 * Basic outbag socket
 * used to authentificate the user on the server
 * returns a Promise.
 *
 * res({ auth { username, password, server }, raw, send(), next() })
 * - the auth object contains the users auth data
 * - the socket is a reference to the socket
 *   in case you do not want the socket to stay open
 *   you might want to run raw.close() after the promise resolved
 * - the send function is a shorthand for raw.send(JSON.stringify(...))
 * - the next function helps you register callbacks for messages
 *
 * There are three reject types:
 * rej({ type "network", error })
 * - the error contains the fetch network error information
 * rej({ type "server", error, msg })
 * - the error contais the JSON parse error message
 * - the msg object contains the message received by the socket
 * rej({ type "auth", json })
 * - the json object contains datat on why the authentification process failed
 *   see the server docs for more
 */
export const auth = ({
  // object containing the auth data
  // the users homeserver
  // e.g. your.home.server
  server,
  // the users password
  // should be hashed for register and for login
  password,
  // the users username
  // e.g. john
  username,
  // the authentification methos
  // one of [ signin, signup, signupOTA ]
  // you have to specify an ota if using signupOTA
  method = "signin",
  // if the server has a user limit
  // the user might need to provide an ota to be able to register
  // the required key will automatically be inserted when ota is provided
  ota,
  // DEFAULT VALUES
  defaults: {
    PORT = 7223,
    SERVER = "jusax.de",
    PATH = "/",
    FORCE_HTTP = false
  } = {},
}) => new Promise((res, rej) => {
  let has_result = false;
  resolve_url({
    url: server,
    protocol: `http${FORCE_HTTP ? '' : 's'}`,
    defaults: {
      PORT, SERVER, PATH, FORCE_HTTP
    }
  })
    .then(uri => {
      uri.protocol = `ws${FORCE_HTTP ? '' : 's'}`;
      let socket = new WebSocket(uri.toString());

      // what to do with the result
      let next = {};
      let next_counter = 0;
      let request_next = (cb = () => { }) => {
        if (Object.keys(next).length == 0) {
          next_counter = 0;
        }
        next[next_counter] = cb;
        return next_counter++;
      }
      let get_next = (id = 0) => {
        if (next[id] != undefined) {
          let dt = next[id];
          next[id] = undefined;
          return dt;
        } else {
          return () => { }
        }
      }

      let ev_m = socket.addEventListener('message', (msg) => {
        try {
          let json = JSON.parse(msg.data);
          let next = get_next(json.id);
          next(json);
        } catch (e) {
          has_result = true;
          socket.close();
          rej({ type: "server", error: e, msg, as_text: 'Server error' });
        }
      });
      let ev_c = socket.addEventListener('close', () => {
        socket.removeEventListener('message', ev_m);
        socket.removeEventListener('close', ev_c);
        socket.removeEventListener('open', ev_o);
        if (!has_result) {
          // socket closed without successful auth
          // probably network error
          rej({ type: "network", error: undefined, as_text: 'Nework error' })
        }
      });
      let ev_o = socket.addEventListener('open', () => {
        socket.send(JSON.stringify({
          id: request_next(json => {
            if (json.state == "error") {
              // login failed
              socket.close();
              rej({
                type: "auth", json, as_text: err_as_text(json, (() => {
                  switch (json.data) {
                    case 'auth':
                      return 'Username or password wrong';
                    case 'ota':
                      return 'Invalid OTA';
                    case 'existence':
                      return 'Username unavailable';
                    case 'config':
                      return 'Server reached user limit';
                  }
                })())
              });
            } else {
              // logged in
              has_result = true;
              res({ auth: { server, username, password }, raw: socket, send: (json = {}) => socket.send(JSON.stringify(json)), next: request_next });
            }
          }),
          act: method,
          data: {
            name: username,
            accountKey: password,
            // automatically insert ota
            ...(!!ota ? { OTA: ota } : {})
          }
        }))
      });
    })
    .catch(e => {
      has_result = true;
      rej({ type: "network", error: e, as_text: 'Nework error' })
    })
});

// shorthand for auth()
// with method=signin
export const signin = ({
  // object containing the auth data
  // the users homeserver
  // e.g. your.home.server
  server,
  // the users password
  // should be hashed for register and for login
  password,
  // the users username
  // e.g. john
  username,
  // DEFAULT VALUES
  defaults: {
    PORT = 7223,
    SERVER = "jusax.de",
    PATH = "/",
    FORCE_HTTP = false
  } = {},
}) => auth({
  server, username, password,
  method: "signin",
  defaults: { SERVER, PORT, PATH, FORCE_HTTP }
});

// shorthand for auth()
// with method=signup
export const signup = ({
  // object containing the auth data
  // the users homeserver
  // e.g. your.home.server
  server,
  // the users password
  // should be hashed for register and for login
  password,
  // the users username
  // e.g. john
  username,
  // DEFAULT VALUES
  defaults: {
    PORT = 7223,
    SERVER = "jusax.de",
    PATH = "/",
    FORCE_HTTP = false
  } = {},
}) => auth({
  server, username, password,
  method: "signup",
  defaults: { SERVER, PORT, PATH, FORCE_HTTP }
});

// shorthand for auth()
// with method="signupOTA"
export const signup_ota = ({
  // object containing the auth data
  // the users homeserver
  // e.g. your.home.server
  server,
  // the users password
  // should be hashed for register and for login
  password,
  // the users username
  // e.g. john
  username,
  // if the server has a user limit
  // the user might need to provide an ota to be able to register
  // the required key will automatically be inserted when ota is provided
  ota,
  // DEFAULT VALUES
  defaults: {
    PORT = 7223,
    SERVER = "jusax.de",
    PATH = "/",
    FORCE_HTTP = false
  } = {},
}) => auth({
  server, username, password,
  method: "signupOTA",
  ota,
  defaults: { SERVER, PORT, PATH, FORCE_HTTP }
});
