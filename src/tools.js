// NOTE: some of the helper functions are taken from the Outbag Server
// and modified to fit the web

// nodejs polyfill
try {
  let {subtle} = (await import("crypto")).webcrypto;
  global.crypto={
    subtle
  }
} catch(e) {}

// checksum sha256
export const sha256 = async (d) => {
  let msgUint8 = new TextEncoder().encode(d);
  let hb = await crypto.subtle.digest('SHA-256', msgUint8);
  let ha = await Array.from(new Uint8Array(hb));
  return decode(ha, "base64");
};

// converts plain text to Uint8Array ([&u8])
// converts base64 to string with type = base64
export const encode = (data, type = "binary") => {
  let dt = data;
  switch (type) {
    case "base64":
      dt = atob(dt);
      dt = dt.split('').map(d=>d.charCodeAt(0));
      break;
  }
  return new Uint8Array(dt);
};
// converts Uint8Array ([&u8]) to plain text
// when type = base64, it will encode the string using base64
export const decode = (data, type="binary") => {
  let str = [...data]
      .map(i=>String.fromCharCode(i))
      .join('');
  switch ( type ) {
    case "base64":
      str = btoa(str);
      break;
  }
  return str;
};

// generates a public and a private key for the user
// might be regenered at every session begin
// given that the certificate expires,
// it might have to be regenerated in between sessions
export const generate_keypair = async () =>  {
  var keyPair = await crypto.subtle.generateKey(
    {
      name: "RSA-PSS",
      modulusLength: 4096,
      publicExponent: new Uint8Array([1, 0, 1]),
      hash: "SHA-256"
    },
    true,
    ["sign", "verify"]
  );
  return {
    private_key: decode(new Uint8Array(await crypto.subtle.exportKey("pkcs8", keyPair.privateKey)), "base64"),
    public_key: decode(new Uint8Array(await crypto.subtle.exportKey("spki", keyPair.publicKey)), "base64")
  };
}

// signs a message using the users private key
export const sign = async (message, privateKey) => {
  var rawdata = encode(message);

  var rawkey = await crypto.subtle.importKey("pkcs8", encode(privateKey, "base64"), { name: "RSA-PSS", hash: "SHA-256" }, true, ["sign"]);
  var step = await crypto.subtle.sign(
    {
      name: "RSA-PSS",
      saltLength: 32,
    },
    rawkey,
    rawdata
  );

  return decode(new Uint8Array(step), "base64");
};
// verifies the signature of a message
export const verify = async (message, signature, publicKey) => {
  var rawdata = encode(message);
  var rawsig = encode(signature, "base64");

  var rawkey = await crypto.subtle.importKey("spki", encode(publicKey, "base64"), { name: "RSA-PSS", hash: "SHA-256" }, true, ["verify"]);
  var step = await crypto.subtle.verify(
    {
      name: "RSA-PSS",
      saltLength: 32,
    },
    rawkey,
    rawsig,
    rawdata
  );

  return step//; ? "valid" : "invalid";
}


/*
 * Tool to automatically generate english text from error
 */
export const err_as_text = (json, fallback='Unknown error') => {
  let err = fallback;
  if (json && json.state=='error' && json.data) {
    switch (json.data) {
      case 'notfound':
        err = 'Endpoint not found';
        break;
      case 'wrongstate':
        err = 'Missing data';
        break;
      case 'data':
        err = 'Invalid data';
        break;
      case 'right':
        err = 'You are not allowed to perform this action';
        break;
      case 'server':
        err = 'Server error';
        break;
      case 'closed':
        err = 'Server cannot be reached';
        break;
    }
  }
  return err;
}
