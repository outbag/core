const WELL_KNOWN_PATH = "/.well-known/outbag/server";

// node.js polyfill
// requires dev dependencies
try {
  global.fetch = (await import("node-fetch")).default;
} catch(_){}

/*
 * Resolves a homesever url
 */
export const resolve_url = ({
  /*
   * the homeserver url provided by the user
   */
  url,
  /*
   * the protocol being used for the request
   * probably either https or wss
   */
  protocol="https",
  defaults: {
    PORT=7223,
    SERVER="jusax.de",
    PATH="/"
  }={},
}) => new Promise((res, rej) => {
  let uri = url || SERVER;
  try {
    uri = new URL('/', url);
    uri.protocol = protocol;
  } catch (_) {
    uri = new URL(protocol + "://" + url);
  }
  uri.pathname = WELL_KNOWN_PATH;
  fetch(uri)
    .then(resp => resp.json())
    .then(({ path=PATH, port=PORT }) => {
      uri.pathname = path;
      uri.port = port || PORT;
      res(uri);
    })
    .catch(_ => {
      if (uri.port == '') {
        uri.port = PORT;
        resolve_url({
          url: uri.toString(),
          protocol,
          defaults: {
            PORT, SERVER, PATH
          }
        })
          .then(url => res(url))
          .catch(_ => rej("Wasn't able to find server on default port"));
      } else {
        rej("Wasn't able to find server using well-known");
      }
    });
});
export default resolve_url;
