import { auth } from './auth.js';

export const add_ota = ({
  // object containing the auth data
  // the users homeserver
  // e.g. your.home.server
  server,
  // the users password
  // should be hashed for register and for login
  password,
  // the users username
  // e.g. john
  username,
  // the data to create
  data: {
    // user requested token
    token=Math.random().toString(),
    // expiration date
    // NOTE: not a best before date
    // token expires once this uts is in the past
    // in seconds
    expires=Date.now(),
    // amount of times the ota can be used
    // -1 for infinite amount of times
    uses=-1
  },
  // DEFAULT VALUES
  defaults: {
    PORT=7223,
    SERVER="jusax.de",
    PATH="/",
    FORCE_HTTP=false
  }={},
}) => new Promise((res, rej)=>{
  auth({
    server, password, username,
    defaults: {
      PORT, SERVER, PATH, FORCE_HTTP
    }
  })
    .then(({ send, next, raw })=>{
      expires = Math.floor(expires);
      send({
        id: next(json=>{
          if (json.state == "error") {
            // could not create ota
            raw.close();
            rej({ type: "creation", json });
          } else {
            raw.close();
            res({ token, expires, uses });
          }
        }),
        act: "addOTA",
        data: {
          token,
          expires,
          usesLeft: uses
        }
      });
    })
    .catch(e=>rej(e))
});
export default add_ota;
